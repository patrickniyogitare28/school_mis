const express = require('express')
const schoolControllers = require('./controllers/school_controllers')
const studentControllers = require('./controllers/student_controllers')
const userControllers = require('./controllers/user_controllers')
const bodyParser = require('body-parser')
const admin = require('./middlewares/admin');
const config = require('config')

const authMiddleware = require('./middlewares/auth')
const auth = require('./controllers/auth')




const app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:true}));

if(!config.get('jwtPrivateKey')) {
    console.log('PRIVATE KEY missing.....')
    process.exit(1)
}

app.get('/',(req,res)=>{
    res.send('Welcome to school management System....').status(200)
})
app.use('/api/schools',authMiddleware,schoolControllers);
app.use('/api/students',authMiddleware,studentControllers);
app.use('/api/users',userControllers)
app.use('/api/auth',auth)

const port = 3000;

app.listen(port,()=>{
    console.log(`Server running on port ${port}....`);
})


