const jwt = require('jsonwebtoken')
const config = require('config')

function auth(req,res,next){

    const token = req.header('x-auth-token');
    if(!token)
    return res.send('Token missing....').status(401)

    try{
        const decoded = jwt.verify(token,config.get('jwtPrivateKey'))
        req.user = decoded;
        next()
    }
    catch(err){
           res.send('Invalid token........').status(401)
    }
}
module.exports = auth;