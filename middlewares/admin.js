
function admin(req,res,next){

    if(!req.user.admin) return res.send('Have no access...').status(403)
    next()
  
  
  }
  module.exports = admin;