require('../models/mongodb')
const mongoose = require('mongoose')
const admin = require('../middlewares/admin')
const {Student,validate} = require('../models/student.model')
const {School} = require('../models/school.model');

const express = require('express')

const router = express.Router()
// const Student = mongoose.model('Student')
// const School = mongoose.model('School')

router.post('/',(req,res)=>{
    insertIntoStudents(req,res);

})

function insertIntoStudents(req,res){
    const {error} = validate(req.body)
    if(error) return res.send(error.details[0].message)

    let checkSchoolId = School.find({_id:req.body.schoolId})
    if(checkSchoolId){

        const {error} = validate(req.body)
        if(error) return res.send(error.details[0].message)

        
        var newStudent = new Student();

        newStudent.name = req.body.name;
        newStudent.email = req.body.email;
        newStudent.schoolId = req.body.schoolId;
        newStudent.gender = req.body.gender;
        newStudent.age = req.body.age;
       
        newStudent.save()
        .then(savedStudent => res.send(savedStudent).status(201))
        .catch(err => res.send(err).status(400))
    }
    else{
        res.send('The school not found').status(404)
    }

   
}

// router.get('/',(req,res)=>{
//     Student.find()
//     .then(allStundents => res.send(allStundents).status(200))
//     .catch(err => res.send(err).status(400))
// })

router.get('/',(req,res) => {
    Student.find()
        .then(gotStudent => res.send(gotStudent))
        .catch(err => res.send(err).status(404));
    });

router.put('/',admin,(req,res)=>{
    updateTheStudent(req,res);
})
function updateTheStudent(req,res){

    Student.findOneAndUpdate({_id:req.body._id},req.body,{new:true})
    .then(updateS => res.send(updateS).status(201))
    .catch(err => res.send(err).status(404))
}

router.delete('/:id',admin,(req,res)=>{
    Student.findByIdAndRemove(req.params.id)
    .then(deletedStudent => res.send(deletedStudent))
    .catch(err => res.send(err).status(400))
})

router.get('/byGender/:gender',(req,res)=>{
Student.find().
    countDocuments({gender:req.params.gender})

    .then(count => res.send(`Total=  ${count}`).status(200))
    .catch(err => res.send(err).status(200))

})

router.get('/:id',(req,res)=>{
    Student.findById(req.params.id)
    .then(studentById => res.send(studentById))
    .catch(err => res.send(err).status(404))
})
//Getting the total of students




router.get('/studSector/:sector',(req,res)=>{
    getFromSector(req.params.sector);
 })
 async function getFromSector(sectorName){
    let i = 0;
    let schoolFromSector = await School.find({sector:sectorName});
    console.log(schoolFromSector)
    let count=0;
      try{
          for(i = 0; i < schoolFromSector.length; i++){
            StudentsfromSector = await Student.find({schoolId:schoolFromSector[i]._id}).countDocuments();
          count +=StudentsfromSector;
        }
           console.log(`Total student: ${count}`)

            return res.sector(`Total student: ${count}`)
        }
        catch(err){
            console.log(err)
        }
   }
 
 
module.exports = router;
