const mongoose = require('mongoose')
const express = require('express');
const admin = require('../middlewares/admin')
const {School,validate}=require('../models/school.model');

require('../models/mongodb');


// const School = mongoose.model('School');
const router = express()

router.post('/',admin,(req,res)=>{
    insertIntoScools(req,res);
})

function insertIntoScools(req,res){

    const {error} = validate(req.body)
    if(error) return res.send(error.details[0].message).send(201)
    
    var newSchool = new School();
    newSchool.name = req.body.name;
    newSchool.abbreviation = req.body.abbreviation;
    newSchool.motto = req.body.motto;
    newSchool.sector=req.body.sector;

    newSchool.save()
   .then(schoolSaved => res.send(schoolSaved).status(201))
   .catch(err => res.send(err).status(400));
}

router.get('/',(req,res)=>{
    School.find()
    .then(foundSchool => res.send(foundSchool).status(200))
    .catch(err => res.send(err).status(404))
})

router.put('/',admin,(req,res)=>{
    updateTheSchool(req,res);
})
function updateTheSchool(req,res){
    School.findOneAndUpdate({_id:req.body._id},req.body,{new:true})
    .then(updateSchool => res.send(updateSchool).status(201))
    .catch(err => res.send(err).status(404))
}
router.delete('/',admin,(req,res)=>{
    School.findByIdAndRemove({_id,name,abbreviation,motto,sector})
    .then(removed => req.send(removed).status(200))
    .then(err => res.send(err).status(404))
})

router.get('/sameSector/:sector',(req,res)=>{
    School.find({sector:req.params.sector})
        .then(courses => res.send(courses))
        .catch(err => res.send(err).status(404));
})

module.exports = router