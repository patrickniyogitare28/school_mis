const bcrypt = require('bcrypt')
const {User} = require('../models/user.model')
const express = require('express')
const Joi = require('joi')

const router = express.Router()

router.post('/jwt',async (req,res) =>{
    const {error} = validateUser(req.body)
    if(error) return res.send(error.details[0].message).status(400)

    let user  = await User.findOne({email:req.body.email})
    if(!user) return res.send('Invalid email or password').status(400)

    const validPassword = await bcrypt.compare(req.body.password,user.password)
    if(!validPassword) return res.send('invalid email or password').status(400)

   return res.send(user.generateAuthToken())
});

router.post('/bcrypt',async(req,res)=>{
    let {error} = validateUser(req.body)
    if(error) return res.send(error.details[0].message).send(400)

    let user  = await User.findOne({email:req.body.email})
   if(!user) return res.send('Invalid email or password 3').status(400)

   let validPassword = await bcrypt.compare(req.body.password,user.password)
   if(!validPassword) return res.send('Invalid email or password 4').status(400)

   return res.send(_.pick(user,['_id','name','email']))
})

function validateUser(req){

    const schema =
  {  email:Joi.string().min(3).max(255).required(),
    password:Joi.string().min(3).max(255).required()}

    return Joi.validate(req,schema)
}
module.exports=router
