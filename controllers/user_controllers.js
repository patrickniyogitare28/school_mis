const {User,validate} = require('../models/user.model')
const hashedPassowrd = require('../utils/hash')
const _=require('lodash')
const express = require('express')
const router = express.Router()

router.get('/',async (req,res)=>{
    const users = await User.find().sort({name: 1})
    res.send(users);
})

router.get('/:email', async (req,res)=>{
    const user = User.find({email: req.params.email})
    res.send(user).status(200)
})

router.post('/',async (req,res)=>{
    let {error} = validate(req.body)

   if(error) return res.send(error.details[0].message).status(400)

 let user = await User.findOne({email:req.body.email})
 if(user) res.send('User is already registered..').status(400)

 user = new User(_.pick(req.body,['name','email','password']))

 const hashed = await hashedPassowrd(user.password)
 user.password = hashed;

  await user.save()
   return res.send(_.pick(user,['_id','name','email']))
})

module.exports = router;