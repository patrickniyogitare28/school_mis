const mongoose = require('mongoose')
const express = require('express');
const jwt = require('jsonwebtoken');
const config = require('config');
const bcrypt = require('bcrypt')
const Joi = require('joi')

var UserShema = new mongoose.Schema({

    name:{
         type: String,
       
         required: true,
         maxlength: 255,
         minlength: 3
    },
  
    email:{
        type: String,
        required: true,
        unique: true,
        maxlength: 255,
        minlength: 3
    },
    password:{
        type:String,
        required:true,
        minlength:3,
       maxlength:255
    },
    isAdmin:{
        type:Boolean,
        default:false,
        required:true
    }
})




UserShema.methods.generateAuthToken = function(){

    const token  =jwt.sign({_id:this._id,name:this.name,email:this.email,isAdmin:this.isAdmin}
        ,config.get('jwtPrivateKey'))
    return token
}

const User = mongoose.model('User',UserShema);

function validateUser(user){
    const schema = {
        name:Joi.string().min(3).max(255).required(),
        email:Joi.string().min(3).max(255).required().email(),
        password:Joi.string().min(3).max(255).required(),
        password:Joi.string().min(3).max(255).required(),
         isAdmin:Joi.required()
    }
    return Joi.validate(user,schema)
}

module.exports.User = User
module.exports.validate = validateUser;

