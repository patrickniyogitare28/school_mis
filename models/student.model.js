const mongoose = require('mongoose')

const Joi = require('@hapi/joi')
Joi.objectId = require('joi-objectid')(Joi)

const StudentSchema = new mongoose.Schema({
    name:{
        type:String,
        minlength:3,
        maxlength:255,
        required:'name required'
    },
    email:{
        type:String,
        minlength:3,
        maxlength:255,
        required:'email is required'
    },
    schoolId:{
        type:String,
        minlength:3,
        maxlength:255,
        required:'name required'
    },
    gender:{
        type:String,
        minlength:3,
        maxlength:255,
        required:'name required'
    },
    age:{
        type:Number,
        required:'name required'
    }

})

function validateStudent(student){

    const schema = 
  { 
       name:Joi.string().min(3).max(255).required(),
       email:Joi.string().min(3).max(255).required(),
       schoolId:Joi.objectId().required(),
       gender:Joi.string().min(3).max(255).required(),
       age:Joi.required()
    }
    return Joi.validate(student,schema)
}
const Student = mongoose.model('Student',StudentSchema);

module.exports.validate=validateStudent;
module.exports.Student=Student;

// const School=mongoose.model('School',schoolSchema);

// module.exports.validate=validateSchool;
// module.exports.School=School;

