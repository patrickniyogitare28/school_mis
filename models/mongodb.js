const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/School_MIS',{
  useNewUrlParser:true,
  useUnifiedTopology:true
})
.then(()=> console.log('Connected to mongodb.....'))
.catch(err => ('failed to connect to mongodb....',err));

require('./school.model');
require('./student.model')
