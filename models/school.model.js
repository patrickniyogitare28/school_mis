
const mongoose = require('mongoose')
const Joi = require('joi');

var schoolSchema = new mongoose.Schema({
    name:{
        type:String,
        minlength:3,
        maxlength:255,
        required:('Name is required')
    },
    abbreviation:{
        type:String,
        minlength:3,
        maxlength:255,
        required:('Abbreviation Is required')

    },
    motto:{
        type:String,
        minlength:3,
        maxlength:255,
        required:('Motto Is required')

        
    },
    sector:{
        type:String,
        minlength:3,
        maxlength:255,
        required:('Sector Is required')

    }

})


function validateSchool(school){
    const schema = {
        name:Joi.string().min(3).max(255).required(),
        abbreviation:Joi.string().min(3).max(255).required(),
        motto:Joi.string().min(3).max(64).required(),
        sector:Joi.string().min(3).max(64).required()
    }
    return Joi.validate(school,schema)
}

 const School=mongoose.model('School',schoolSchema);

module.exports.validate=validateSchool;
module.exports.School=School;


